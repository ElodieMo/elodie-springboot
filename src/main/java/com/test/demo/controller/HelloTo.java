package com.test.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloTo {
    @GetMapping("/hello-to")
    @ResponseBody
    public String showTestMessage(
            @RequestParam(required = false, defaultValue = "Simplon") String name) {
        return "Hello" + " " + name;
    }
}